﻿using turbonomicsapi_console.API;
using turbonomicsapi_console.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using Newtonsoft.Json;
using System.Net;

namespace turbonomicsapi_console
{
    class Program
    {
        static void Main(string[] args)
        {
            Authentication auth = new Authentication();
            auth.GetAuthToken();

            Targets targets = new Targets{AuthToken = auth.AuthToken, BaseURL = auth.BaseURL};
            Tags tags = new Tags{AuthToken = auth.AuthToken, BaseURL = auth.BaseURL};
            Policies policies = new Policies{AuthToken = auth.AuthToken, BaseURL = auth.BaseURL};
            
            var targetList = targets.GetTargets();
            foreach(var target in targetList){
                Console.WriteLine("Target: " + target.displayName + "\nHealth Status: " + target.healthSummary.healthState 
                + "\nCategory: " + target.category + "\nuuid: " + target.uuid + "\n");
            }

            var targetHealthList = targets.GetHealthStatusForTargets("NORMAL");

            var tagList = tags.GetTags();
            foreach(var tag in tagList){
                Console.WriteLine("Key: " + tag.key);

                var valueCount = 1;
                foreach(var value in tag.values){
                    Console.WriteLine("Value{0}: {1}", valueCount, value);
                    valueCount++;
                }
                Console.WriteLine();
            }

            System.Console.WriteLine("Return entities for tag. Input tag search term (Case Sensitive)");
            bool containsItem = true;

            do{
                var entityInput = Console.ReadLine();
                containsItem = tagList.Any(item =>item.key == entityInput);
                if(!containsItem){
                    System.Console.WriteLine("\nInput a valid tag (Case Sensitive)");
                }
                else{
                    var entitiesList = tags.GetTagEntities(entityInput);
                }

            }while(!containsItem);

            var policyList = policies.GetPolicies();
            
            Console.ReadKey();
        }
    }
}