namespace turbonomicsapi_console.Models
{
   // Root myDeserializedClass = JsonConvert.DeserializeObject<List<Root>>(myJsonResponse);
    public class TargetsHealth
    {
        public string errorText { get; set; }
        public string healthCategory { get; set; }
        public string healthClassDiscriminator { get; set; }
        public string uuid { get; set; }
        public string targetName { get; set; }
        public string timeOfFirstFailure { get; set; }
        public List<TargetErrorDetail> targetErrorDetails { get; set; }
        public string targetStatusSubcategory { get; set; }
        public string healthState { get; set; }
    }

    public class TargetErrorDetail
    {
        public string targetErrorDetailsClass { get; set; }
    }

}
