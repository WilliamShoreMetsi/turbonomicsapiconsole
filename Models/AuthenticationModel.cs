namespace turbonomicsapi_console.Models
{
    class AuthenticationModel
    {
        public string uuid { get; set; }
        public string username { get; set; }
        public string rolename { get; set; }
        public List<Role> roles { get; set; }
        public string loginProvider { get; set; }
        public string authToken { get; set; }
        public string showSharedUserSC { get; set; }
    }

    public class Role
    {
        public string name { get; set; }
    }
}
