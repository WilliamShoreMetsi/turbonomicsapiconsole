namespace turbonomicsapi_console.Models
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<List<Root>>(myJsonResponse);
    public class DiscoveredBy
    {
        public string uuid { get; set; }
        public string displayName { get; set; }
        public string category { get; set; }
        public string type { get; set; }
        public bool @readonly { get; set; }
    }

    public class Provider
    {
        public string uuid { get; set; }
        public string displayName { get; set; }
        public string className { get; set; }
    }

    public class Entity
    {
        public string uuid { get; set; }
        public string displayName { get; set; }
        public string className { get; set; }
        public double priceIndex { get; set; }
        public string state { get; set; }
        public string severity { get; set; }
        public DiscoveredBy discoveredBy { get; set; }
        public SeverityBreakdown severityBreakdown { get; set; }
        public List<Provider> providers { get; set; }
        public string environmentType { get; set; }
        public EntityTags tags { get; set; }
        public VendorIds vendorIds { get; set; }
        public string staleness { get; set; }
    }

    public class SeverityBreakdown
    {
        public int NORMAL { get; set; }
        public int? MINOR { get; set; }
        public int? CRITICAL { get; set; }
    }
    public class EntityTags
    {

        
    }


    public class VendorIds
    {
        public string name { get; set; }
    }


}
