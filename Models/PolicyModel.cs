namespace turbonomicsapi_console.Models
{
        // Root myDeserializedClass = JsonConvert.DeserializeObject<List<Root>>(myJsonResponse);
    public class ConsumerGroup
    {
        public string uuid { get; set; }
        public string displayName { get; set; }
        public string className { get; set; }
        public int membersCount { get; set; }
        public int entitiesCount { get; set; }
        public string groupType { get; set; }
        public string environmentType { get; set; }
        public bool isStatic { get; set; }
        public string logicalOperator { get; set; }
        public List<string> memberUuidList { get; set; }
        public bool temporary { get; set; }
        public int activeEntitiesCount { get; set; }
        public string cloudType { get; set; }
        public Source source { get; set; }
        public List<string> memberTypes { get; set; }
        public List<string> entityTypes { get; set; }
        public string groupOrigin { get; set; }
        public string groupClassName { get; set; }
    }

    public class Policy
    {
        public string uuid { get; set; }
        public string displayName { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public bool enabled { get; set; }
        public string commodityType { get; set; }
        public ConsumerGroup consumerGroup { get; set; }
        public string providerEntityType { get; set; }
    }

    public class Source
    {
        public string uuid { get; set; }
        public string displayName { get; set; }
        public string category { get; set; }
        public string type { get; set; }
        public bool @readonly { get; set; }
    }


}
