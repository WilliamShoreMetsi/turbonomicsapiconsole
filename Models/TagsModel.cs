namespace turbonomicsapi_console.Models
{
        // Root myDeserializedClass = JsonConvert.DeserializeObject<List<Root>>(myJsonResponse);
        public class Tag
        {
            public string key { get; set; }
            public List<string> values { get; set; }
        }
}
