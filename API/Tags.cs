using Newtonsoft.Json;
using RestSharp;
using turbonomicsapi_console.Models;

namespace turbonomicsapi_console.API
{

    public class Tags
    {
        public string AuthToken { get; set; }
        public string BaseURL { get; set; }

        List<Tag> tagsList = new List<Tag>();
        public List<Tag> GetTags(){

            //ignore ssl for purposes of testing. Call will fail TLS/SSL check otherwise
            var options = new RestClientOptions(BaseURL) {
            RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true
            };

            var client = new RestClient(options);
            var request = new RestRequest(BaseURL + "tags", Method.Get);
            request.AddHeader("cookie", AuthToken);
            var response = client.Execute(request);
            tagsList = JsonConvert.DeserializeObject<List<Tag>>(response.Content);

            return tagsList;
        }

        public List<Entity> GetTagEntities(string tag){

            //ignore ssl for purposes of testing. Call will fail TLS/SSL check otherwise
            var options = new RestClientOptions(BaseURL) {
            RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true
            };

            var client = new RestClient(options);
            var request = new RestRequest(BaseURL + "tags/" + tag + "/entities", Method.Get);
            request.AddHeader("cookie", AuthToken);
            var response = client.Execute(request);
            var entitiesList = JsonConvert.DeserializeObject<List<Entity>>(response.Content);

            //var t = response.Content;
            return entitiesList;
        }
    }
}