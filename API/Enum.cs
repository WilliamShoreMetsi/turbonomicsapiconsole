public enum HealthStatus
{
    NORMAL,
    MINOR,
    MAJOR,
    CRITICAL
}