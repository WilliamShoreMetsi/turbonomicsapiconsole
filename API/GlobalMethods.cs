using RestSharp;

namespace turbonomicsapi_console.API
{
    public class GlobalMethods
    {
        public bool checkHeadersForCursor(RestResponse response){

            var isEmpty = false;

                foreach(var header in response.Headers){
                    if(header.Name == "X-Next-Cursor"){
                        if(string.IsNullOrEmpty(header.Value.ToString())){
                            isEmpty = true;
                        }
                    }
                }

            return isEmpty;
        }
    }
}
