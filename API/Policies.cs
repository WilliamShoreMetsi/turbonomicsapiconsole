using Newtonsoft.Json;
using RestSharp;
using turbonomicsapi_console.Models;

namespace turbonomicsapi_console.API
{    public class Policies
    {
        public string AuthToken { get; set; }
        public string BaseURL { get; set; }

        List<Policy> policyList = new List<Policy>();
            public List<Policy> GetPolicies(){

                var options = new RestClientOptions(BaseURL) {
                RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true
                };

                var client = new RestClient(options);
                var request = new RestRequest(BaseURL + "policies", Method.Get);
                request.AddHeader("cookie", AuthToken);
                var response = client.Execute(request);
                policyList = JsonConvert.DeserializeObject<List<Policy>>(response.Content);

            
            return policyList;
            }
    }
}