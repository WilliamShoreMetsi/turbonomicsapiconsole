using Newtonsoft.Json;
using RestSharp;
using turbonomicsapi_console.Models;

namespace turbonomicsapi_console.API
{
    public class Targets
    {      

        public string AuthToken { get; set; }
        public string BaseURL { get; set; }
        public List<TargetRoot> GetTargets()
        {
            var targetList = new List<TargetRoot>();
            var gMethod = new GlobalMethods();
            var paging = 1;
            var xNextCursor = 0;
            var uri = "";
            var endLoop = false;

            do{
                //ignore ssl for purposes of testing. Call will fail TLS/SSL check otherwise
                var options = new RestClientOptions(BaseURL) {
                RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true
                };

                var client = new RestClient(options);
                uri = (BaseURL + "targets?cursor=" + xNextCursor + "&limit=" + paging + "&ascending=true");
                var request = new RestRequest(uri, Method.Get);
                request.AddHeader("cookie", AuthToken);
                var response = client.Execute(request);

                //if cursor header is empty, return true to end the loop
                endLoop = gMethod.checkHeadersForCursor(response);
                xNextCursor += paging;

                targetList.AddRange(JsonConvert.DeserializeObject<List<TargetRoot>>(response.Content));
                
            }while(!endLoop);
           
            return targetList;
        }

        public List<TargetsHealth> GetHealthStatusForTargets(string health){

            var targetList = new List<TargetsHealth>();
            var gMethod = new GlobalMethods();
            var paging = 1;
            var xNextCursor = 0;
            var uri = "";
            var endLoop = false;

            do{
                //ignore ssl for purposes of testing. Call will fail TLS/SSL check otherwise
                var options = new RestClientOptions(this.BaseURL) {
                RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true
                };

                var client = new RestClient(options);
                uri = (BaseURL + "targets/health?health_state=" + health + "cursor=" + xNextCursor + "&limit=" + paging + "&ascending=true");
                var request = new RestRequest(uri, Method.Get);
                request.AddHeader("cookie", this.AuthToken);
                var response = client.Execute(request);

                //if cursor header is empty, return true to end the loop
                endLoop = gMethod.checkHeadersForCursor(response);
                xNextCursor += paging;

                targetList.AddRange(JsonConvert.DeserializeObject<List<TargetsHealth>>(response.Content));
                
                return targetList;
            }while(!endLoop);
        }
    }
}
