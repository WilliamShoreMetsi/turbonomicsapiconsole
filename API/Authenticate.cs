using RestSharp;
using turbonomicsapi_console.Models;

namespace turbonomicsapi_console.API
{
    public class Authentication
    {
        public string AuthToken { get; set; }
        public string BaseURL { get; set; } = "https://10.22.66.3/api/v3/";
        public string Username { get; set; } = "administrator";
        public string Password { get; set; } = "M3tsiT3ch!23";

        public void GetAuthToken()
        {
            //ignore ssl for purposes of testing. Call will fail TLS/SSL check otherwise
            var options = new RestClientOptions(BaseURL) {
            RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true
            };

            var client = new RestClient(options);
            var request = new RestRequest(BaseURL + "login", Method.Post);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("username", Username);
            request.AddParameter("password", Password);
            var response = client.Execute(request);
            
            foreach(var item in response.Headers)
            {
               if(item.Name == "Set-Cookie")
                {
                    AuthToken = item.Value.ToString();
                    AuthToken = AuthToken.Remove(AuthToken.IndexOf(";"));
                }
            }
        }
    }
}
